<p>Hi <?=$name?>,<br /> 
You have registered an event on https://fsfe.org/events/tools/eventregistration</p>

<p>Below is the list of the information you gave.</p>
<ul>
	<li>Name: <?=$name?></li>
	<li>Email: <?=$email?></li>
	<li>Event Title: <?=$title?></li>
	<li>Location: <?=$location?></li>
</ul>
 
<p>You find all other information in the xml file attached. It will be uploaded
in the next 24 hours. If you like to withdraw your event or in case you like to change some information,
please contact contact@fsfe.org</p>
 
<p>Thanks,<br />
your FSFE team</p>
